/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author JUANCHO
 */
@Entity
@Table(name="chasises")

public class Chasis implements Serializable {
    @Id
    @Column
    private Long id;
    @Column
    private String codigo;
    
    @OneToOne(mappedBy = "chasis")
    private Motor motor;

    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    public Chasis(){
    };

    public Chasis(Long id, String codigo) {
        this.id = id;
        this.codigo = codigo;
    }

    
    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }
    
    @Override
    public String toString() {
        return "Chasis{" + "id=" + id + ", codigo=" + codigo + '}';
    }

}
