/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "empleados")

public class Empleado implements Serializable {

    @Id
    @Column(name = "cedula")
    private Long CC;

    public Empleado(Long CC, String nombres, String apellidos) {
        this.CC = CC;
        this.nombres = nombres;
        this.apellidos = apellidos;
    }

    private String nombres;    
    private String apellidos;
    
    @Column(name = "tel_movil")
    private Long telMovil;
    
    @Column(name = "tel_fijo")
    private int telFijo;
    
   /* @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "SEGURIDAD_ID")
    private SeguridadSocial seguridadSocial;*/
    
    private String eps;
    
    @Column(name = "tipo_contrato")
    private String tipoDeContrato;
    
    @Column(name = "nivel_academico")
    private String nivelAcademico;
    
    @Column(name = "fecha_ingreso")
    private String fechaIngreso;
    
    @Column(name = "fecha_actualizacion")
    private String fechaActualizacion;
    

    public Empleado() {
    }

    public Empleado(Long CC, String nombres, String apellidos, Long telMovil, int telFijo, String eps, String tipoDeContrato, String nivelAcademico) {
        this.CC = CC;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telMovil = telMovil;
        this.telFijo = telFijo;
        //this.seguridadSocial = seguridadSocial;
        this.eps = eps;
        this.tipoDeContrato = tipoDeContrato;
        this.nivelAcademico = nivelAcademico;
        //this.fechaIngreso = fechaIngreso;
        //this.fechaActualizacion = fechaActualizacion;
    }
    public Long getCC() {
        return CC;
    }

    public void setCC(Long CC) {
        this.CC = CC;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEps() {
        return eps;
    }

    public void setEps(String eps) {
        this.eps = eps;
    }

    public String getTipoDeContrato() {
        return tipoDeContrato;
    }

    public void setTipoDeContrato(String tipoDeContrato) {
        this.tipoDeContrato = tipoDeContrato;
    }

    public String getNivelAcademico() {
        return nivelAcademico;
    }

    public void setNivelAcademico(String nivelAcademico) {
        this.nivelAcademico = nivelAcademico;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Long getTelMovil() {
        return telMovil;
    }

    public void setTelMovil(Long telMovil) {
        this.telMovil = telMovil;
    }

    public int getTelFijo() {
        return telFijo;
    }

    public void setTelFijo(int telFijo) {
        this.telFijo = telFijo;
    }
    
      @Override
    public String toString() {
        return "Empleado{" + "CC=" + CC + ", nombres=" + nombres + ", apellidos=" + apellidos + ", telMovil=" + telMovil + ", telFijo=" + telFijo + ", eps=" + eps + ", tipoDeContrato=" + tipoDeContrato + ", nivelAcademico=" + nivelAcademico + ", fechaIngreso=" + fechaIngreso + ", fechaActualizacion=" + fechaActualizacion + '}';
    }
}