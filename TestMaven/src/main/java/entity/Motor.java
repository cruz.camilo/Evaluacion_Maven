/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="motores")
public class Motor implements Serializable {
    
    @Id
    @Column(name="motor_id")
    private Long id;
    @Column(name="num_valvulas")
    private Integer numValvulas;
    @Column
    private Integer cilindraje;
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "chasis_id")
    private Chasis chasis;

   

    public Motor(Long id, Integer numValvulas, Integer cilindraje, Chasis chasis) {
        this.id = id;
        this.numValvulas = numValvulas;
        this.cilindraje = cilindraje;
        this.chasis = chasis;
    }
    
   
    
    public Motor(){
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumValvulas() {
        return numValvulas;
    }

    public void setNumValvulas(Integer numValvulas) {
        this.numValvulas = numValvulas;
    }

    public Integer getCilindraje() {
        return cilindraje;
    }

    public void setCilindraje(Integer cilindraje) {
        this.cilindraje = cilindraje;
    }
    
    public Chasis getChasis() {
        return chasis;
    }

    public void setChasis(Chasis chasis) {
        this.chasis = chasis;
    }
    
     @Override
    public String toString() {
        return "Motor{" + "id=" + id + ", numValvulas=" + numValvulas + ", cilindraje=" + cilindraje + ", chasis=" + chasis + '}';
    }
}
