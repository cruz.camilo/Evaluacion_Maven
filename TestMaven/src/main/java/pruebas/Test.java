/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebas;

import entity.Chasis;
import entity.Empleado;
import entity.Motor;
import java.time.LocalDate;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author JUANCHO
 */
public class Test {

    private static EntityManager manager;
    private static EntityManagerFactory emf;

    public static void main(String[] args) {

        emf = Persistence.createEntityManagerFactory("Persistence1");
        manager = emf.createEntityManager();
        Crud t = new Crud();
        /*
        manager.getTransaction().begin(); 
        Empleado e;
        e =  manager.find(Empleado.class, 103L);
        System.out.println(e);
        
        
        manager.getTransaction().commit();
        manager.close();
        */
        //TEST CREAR hay problemas al crear varios objetos a la vez.
        //t.Crear(6L, 4, 400, new Chasis(7L, "xxs"));
        
        //TEST UPDATE
        //Motor m = new Motor();
        //t.Update(m, 7L, 30);
        
        //TEST DELETE
        //Motor m = new Motor();
        //t.Delete(m, 6L);
        
        //TEST READ
        //Motor m = new Motor();
        //t.Read(m, 4L);
        
        // TEST READ ALL
        t.ReadAll();
        
    }
}
