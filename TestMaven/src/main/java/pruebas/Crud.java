/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebas;

import entity.Chasis;
import entity.Empleado;
import entity.Motor;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author JUANCHO
 */
public class Crud {
    private static EntityManager manager;
    private static EntityManagerFactory emf;
    
    public Crud() {        
    emf = Persistence.createEntityManagerFactory("Persistence1");
    manager = emf.createEntityManager();
    }

    //public Motor(Long id, Integer numValvulas, Integer cilindraje, Chasis chasis) {
    
    public void Crear(Long id, Integer numValvulas, Integer cilindraje, Chasis chasis){
        manager.getTransaction().begin();  
        Motor m = new Motor(id, numValvulas, cilindraje, chasis);
        manager.persist(m);
        manager.getTransaction().commit();
        manager.close();
    }
    
    public void Update(Motor motor, Long id, Integer numValvulas) {
        manager.getTransaction().begin();  
        motor = manager.find(Motor.class, id);
        motor.setNumValvulas(numValvulas);
        manager.getTransaction().commit();
        manager.close();
    }
    
    public void Delete(Motor motor, Long id){
        manager.getTransaction().begin();
        motor = manager.find(Motor.class, id);                        
        manager.remove(motor);                                
        manager.getTransaction().commit();
        manager.close();
    }
    
    public void Read(Motor motor, Long id){
        motor = manager.find(Motor.class, id);
        System.out.println(motor);
    }
    
    public void ReadAll(){
        List<Motor> motores = new ArrayList();
        motores = manager.createQuery("FROM Motor").getResultList();
        System.out.println(motores);
        System.out.println(motores.size());
    }
}
